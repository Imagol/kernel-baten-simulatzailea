#ifndef DYNAMIC_H_
#define DYNAMIC_H_

typedef struct node_list{
    void *data;
    struct node_list *next;
    struct node_list *prev;

}node_list;

typedef struct list_t{

    long length;    
    
    int t_size;
    
    struct node_list *head;
    struct node_list *tail;
}list_t;

void list_initialized(list_t *list, int t_size);
long list_length(list_t *list);
void list_get_elem(list_t *list, void *elementu);
void list_insert_i(list_t *list, void *elem);

#endif
