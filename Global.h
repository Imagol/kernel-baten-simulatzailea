#ifndef GLOBALAK_H_
#define GLOBALAK_H_
#define hari_kop 5
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "Dynamic.h"
#include <string.h>



extern int MAX;
extern int tik;
extern int seinalea;
extern pthread_mutex_t mutex1;
extern pthread_cond_t cond1;
extern pthread_cond_t cond2;
extern pthread_mutex_t PCB_kontrol;
extern int tenp_kop;
extern int CORE;
extern int THREAD;
extern int QUANTUM;
extern int done;
extern int banaketa_bukatu;
extern struct Prozesuak prozesu;
extern struct Prozesuak lag;
extern struct CPU cpu;
extern list_t lista_pcb;


struct Runqueue{
  //ze ilaratan gauden jakiteko
  int status;

  struct list_t a[99];

  struct list_t b[99];

};

struct Prozesuak{ 
  //identifikatzailea
  int pid;
  //lehentasuna
  int pref;
  //exekuzioan dago?
  int exe;
  //exekuzio denbora
  int denb;
  //exekuzio pasaden denbora
  int pasadenb;
  //barruan dagoen edo ez jakiteko
  int barruan;
}; 

struct Thread {

  int threadid;

  int useable;

  int quantum;

  struct Prozesuak *prozesu;

};

struct Core{

  int coreid;

  struct Thread *threads;

  struct Runqueue plan;

};


struct CPU {

  int core_kop;

  struct Core *coreak;

};

void *hari_timer(void *t_p);

void *hari_clock();

void *hari_scheduler();

void *hari_pcb();
void *timer_PCB();


struct hariaren_parametroak{

  int  haria_id;

};

struct timer_parametroak{

   int denb;
   
};

void banaketa(struct list_t *lista_pcb, struct CPU *cpu);

#endif
