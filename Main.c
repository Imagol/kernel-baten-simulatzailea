#include "Global.h"
#include <pthread.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond1= PTHREAD_COND_INITIALIZER;
pthread_cond_t cond2 = PTHREAD_COND_INITIALIZER;
pthread_mutex_t PCB_kontrol;
struct CPU cpu;

int opt;
int pcb_rand, timer_count, lagun;

int CORE=90;
int THREAD=90;
int QUANTUM=10000;

list_t lista_pcb;

int done,tenp_kop, banaketa_bukatu;


void sortu_hariak(int timer_count, int pcb_rand){
    printf("Hariak sortzen \n");
    int err;
    pthread_t *hariak;
    struct timer_parametroak *t_p;
    t_p=malloc(timer_count * sizeof(struct timer_parametroak));
    t_p[0].denb=timer_count;
    hariak = malloc(hari_kop * sizeof(pthread_t)); 

    printf("Lehenengo haria sortu \n");

    err = pthread_create(&hariak[0],NULL, hari_timer,(void*)&t_p[0]);;
    if(err==0){
        printf("Haria timer sortu da\n");}
    
    err = pthread_create(&hariak[1], NULL, hari_clock,NULL);;
    if (err==0){
        printf("Haria clock sortu da\n");}

    err = pthread_create(&hariak[2], NULL, hari_pcb,NULL);;
    if (err==0){
        printf("Haria pcb sortu da\n");}

    err = pthread_create(&hariak[3], NULL, hari_scheduler,NULL);;
    if (err==0){
        printf("Haria scheduler sortu da\n");}

    int s;
    for (s=0;s < hari_kop;s++) {
        pthread_join(hariak[s], NULL);
    }
    free(hariak);
    free(t_p);
}

int main(int argc, char *argv[]){
    pthread_mutex_init(&PCB_kontrol, 0);

    while ((opt=getopt(argc, argv, "c:t:h:r:"))!=-1){
   

        switch(opt)
        {
            case 't':   
                lagun=atoi(optarg);
                printf("Timer 1: %d \n",lagun);
                timer_count=lagun;
                break;
            case 'c':
                lagun=atoi(optarg);
                printf("Core kopurua: %d \n",lagun);
                CORE=lagun;
                break;
            case 'h':
                lagun=atoi(optarg);
                printf("Thread kopurua: %d \n",lagun);
                THREAD=lagun;
                break;
            case 'r':
                lagun=atoi(optarg);
                printf("Random num: %d \n",lagun);
                pcb_rand=lagun;
                break;
            case '?':
                printf("unknown option %c \n",optopt);
                break; 
        }
    }
    
    cpu.coreak=malloc(CORE*sizeof(struct Core));

    cpu.core_kop=CORE;
    for (int i=0;i<CORE;i++){
        //printf("aaa"); ya entra
        cpu.coreak[i].coreid=i;
        cpu.coreak[i].threads=malloc(THREAD*sizeof(struct Thread));
        cpu.coreak[i].plan.status=0;
        for(int m=0;m<90;m++){
            //printf("otro for"); ya entra
            list_initialized(&cpu.coreak[i].plan.a[m], sizeof(struct Prozesuak));
            list_initialized(&cpu.coreak[i].plan.b[m], sizeof(struct Prozesuak));
        }
        for (int j = 0; j < THREAD; j++){
            //printf("thread for"); ya entra
            cpu.coreak[i].threads[j].threadid=j;
            cpu.coreak[i].threads[j].useable=1;
            cpu.coreak[i].threads[j].quantum=QUANTUM;
        }
    }

    printf("Programa hasi da \n");
    printf("CPUa sortu da core batean id %d \n", cpu.coreak[1].coreid);

    sortu_hariak(timer_count, pcb_rand);
    printf("Hariak sortu dira \n");

    return(0);
}
