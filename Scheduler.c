#include "Global.h"

void banaketa(list_t *lista_pcb, struct CPU *cpu);
void exekuzioa(struct CPU *cpu);
void exek(struct CPU *cpu);

struct Prozesuak lag;

void *hari_scheduler(){

    printf("Schedulerra martxan dago\n");

    while(1){

        banaketa(&lista_pcb, &cpu);
    
        exekuzioa(&cpu);
    }
	pthread_exit(NULL);

}

void banaketa(list_t *lista_pcb, struct CPU *cpu){
    printf("Banaketa martxan dago \n");

    printf("While-an sartuko da \n");
    
    while(1){
        printf("While-an sartu da"); //Ez da while honetan sartzen
        if(cpu->coreak[0].plan.status==0){
            //printf("scheduler while status"); hemen sartzen da
            printf("%d core kop", cpu->core_kop);
            printf("%d",cpu->core_kop);
            for(int i=0;i<cpu->core_kop;i++){
                printf("for-aren barruan"); //hemen ez da sartzen
                //printf("AAAAA");
                list_get_elem(lista_pcb, &lag);
                printf("%d garren corea \n", i);
                list_insert_i(&(cpu->coreak[i].plan.a[lag.pref]),&lag);
            }
            //printf("for-aren kanpoan"); egiten du
            pthread_mutex_lock(&PCB_kontrol);
            if(banaketa_bukatu==1){
                printf("Banaketa egin du\n");
                banaketa_bukatu=0;
                pthread_mutex_unlock(&PCB_kontrol);
                break;
        }
        pthread_mutex_unlock(&PCB_kontrol);
            
        }else{

            for(int i=0;i<cpu->core_kop;i++){
                list_get_elem(lista_pcb, &lag);
                printf("%d garren corea \n", i);
                list_insert_i(&(cpu->coreak[i].plan.b[lag.pref]),&lag);
            }
            pthread_mutex_lock(&PCB_kontrol);
            if(banaketa_bukatu==1){
                printf("Banaketa egin du\n");
                banaketa_bukatu=0;
                pthread_mutex_unlock(&PCB_kontrol);
                break;
            }
            pthread_mutex_unlock(&PCB_kontrol);
        }
    }
}

void exekuzioa(struct CPU *cpu){
    printf("Exekuzioa martxan dago\n");
    int t=THREAD;
    int l=0;
    while(l!=99){
        if(cpu->coreak[0].plan.status==0){  
            for(int i=0;i<cpu->core_kop;i++){
                for(int j=0; j<t;j++){
                    if(cpu->coreak[i].threads[j].useable==1){
                        if((list_length(&(cpu->coreak[i].plan.a[l]))==0) && (l<99)){
                        l++;                
                    }
                    list_get_elem(&(cpu->coreak[i].plan.a[l]), &lag);
                    memcpy(cpu->coreak[i].threads[j].prozesu, &lag, sizeof(lag));
                    cpu->coreak[i].threads[j].prozesu->exe=1;
                    cpu->coreak[i].threads[j].prozesu->barruan=1;
                    cpu->coreak[i].threads[j].useable=0;
                    if((list_length(&(cpu->coreak[i].plan.a[l]))==0) && (l==99 )){
                        l++;                
                    }
                }
            }  
        }
        exek(cpu);
        } else {
            for(int i=0;i<cpu->core_kop;i++){
                for(int j=0; j<t;j++){
                    if(cpu->coreak[i].threads[j].useable==1){
                        if((list_length(&(cpu->coreak[i].plan.b[l]))==0) && (l<99)){
                            l++;                
                        }
                        list_get_elem(&(cpu->coreak[i].plan.b[l]), &lag);
                        memcpy(cpu->coreak[i].threads[j].prozesu, &lag, sizeof(lag));
                        cpu->coreak[i].threads[j].prozesu->exe=1;
                        cpu->coreak[i].threads[j].prozesu->barruan=1;
                        cpu->coreak[i].threads[j].useable=0;
                        if((list_length(&(cpu->coreak[i].plan.b[l]))==0) && (l==99)){
                            l++;                
                        }
                    }
                }     
            }
        exek(cpu);
        }
    }  
}



void exek(struct CPU *cpu){
    int lehen;
    int laguntzaile=QUANTUM;
    while(laguntzaile!=0){
    for(int i=0;i<cpu->core_kop;i++){
         for(int j=0; j<THREAD;j++){
            if(cpu->coreak[i].threads[j].prozesu->pasadenb==0){
                cpu->coreak[i].threads[j].useable=1;            
            } else {
                cpu->coreak[i].threads[j].prozesu->pasadenb --;
            }
        }
    }
    laguntzaile--;
    }
    if(cpu->coreak[0].plan.status==0){
        for(int i=0;i<cpu->core_kop;i++){
            for(int j=0; j<THREAD;j++){
                if(cpu->coreak[i].threads[j].prozesu->pasadenb!=0){
                    lehen=cpu->coreak[i].threads[j].prozesu->pref;
                    list_insert_i(&(cpu->coreak[i].plan.b[lehen]), cpu->coreak[i].threads[j].prozesu); 
                }
                cpu->coreak[i].threads[j].useable=1;
            }
            cpu->coreak[i].plan.status=1;    
        }
    } else {
        for(int i=0;i<cpu->core_kop;i++){
            for(int j=0; j<THREAD;j++){
                if(cpu->coreak[i].threads[j].prozesu->pasadenb!=0){
                    lehen=cpu->coreak[i].threads[j].prozesu->pref;
                    list_insert_i(&(cpu->coreak[i].plan.a[lehen]), cpu->coreak[i].threads[j].prozesu);
                }
                cpu->coreak[i].threads[j].useable=1;
            }
            cpu->coreak[i].plan.status=0;
        }
    }
}