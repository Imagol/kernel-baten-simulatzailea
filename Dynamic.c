#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct node_list {
    void *data;
    struct node_list *next;
    struct node_list *prev;
} node_list;

typedef struct list_t {
    long length;
    int t_size;
    struct node_list *head;
    struct node_list *tail;
} list_t;

void list_initialized(list_t *list, int t_size) {
    list -> length = 0;
    list -> head = NULL;
    list -> tail = NULL;
    list -> t_size = t_size;
}

long list_length(list_t *list) {
    return list -> length;
}

void list_get_elem(list_t *list, void *elementu) {
    if(list -> length > 1) {
        memcpy(elementu, list -> tail -> data, list -> t_size);
        free(list -> tail -> data);
        list -> tail = list -> tail -> prev;
        free(list -> tail -> next);
        list -> tail -> next = NULL;
        list -> length--;
    } else {
        memcpy(elementu, list -> tail -> data, list -> t_size);
        free(list -> tail -> data);
        free(list -> tail);
        list -> tail = NULL;
        list -> length--;
    }
}

void list_insert_i(list_t *list, void *elem) {
    node_list *new;
    new = malloc(sizeof (node_list));
    new -> data = malloc((list -> t_size));
    memcpy(new -> data, elem, list -> t_size);
    new -> next = NULL;
    new -> prev = NULL;

    if((list -> length = 0)) {
        list -> head = new;
        list -> tail = new;
    } else {
        new -> next = list -> head;
        new -> prev = NULL;
        list -> head -> prev = new;
        list -> head = new;
    }
    list -> length++;
}